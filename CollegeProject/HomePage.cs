﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace CollegeProject
{


    public partial class HomePage : Form
    {



        private int MAX = 256;      // max iterations
        private double SX = -2.025; // start value real
        private double SY = -1.125; // start value imaginary
        private double EX = 0.6;    // end value real
        private double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private int j;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private bool cycleForwards = true;
        private static float xy;
        private HSBColor HSBCol = new HSBColor();

        private Bitmap img;
        private Graphics g1;
        private Cursor c1, c2;
        private Pen pen, pen1;
        bool isMousePressed;
        Rectangle rect = new Rectangle(0, 0, 0, 0);


        public HomePage()
        {
            InitializeComponent();
            init();
            start();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gform = e.Graphics;
            gform.DrawImage(img, 0, 0, x1, y1);
            pen1 = new Pen(Color.White);
            if (isMousePressed == true)
            {
                if (rectangle)
                {
                    gform.DrawRectangle(pen1, rect);
                    rectangle = true;
                    
                    if (xs < xe)
                    {
                        if (ys < ye)
                        {
                            gform.DrawRectangle(pen1, xs, ys, (xe - xs), (ye - ys));
                            Invalidate();
                           
                        }

                        else gform.DrawRectangle(pen1, xs, ye, (xe - xs), (ys - ye));
                    }
                    else
                    {
                        if (ys < ye) gform.DrawRectangle(pen1, xe, ys, (xs - xe), (ye - ys));
                        else gform.DrawRectangle(pen1, xe, ye, (xs - xe), (ys - ye));
                    }
                }
                else if (isMousePressed == false)
                {
                    rectangle = false;
                }

            }
            else
            {
                gform.DrawImageUnscaled(img, 0, 0);

            }
            if (rectangle == false)
            {
                Invalidate();
            }
        }

        private void PBimg_MouseDown(object sender, MouseEventArgs e)
        {

            if (action)
            {

                xs = e.X;
                ys = e.Y;
                isMousePressed = true;



            }
        }

        private void PBimg_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMousePressed)
            {
                
                if (action)
                {
                    
                    xe = e.X;
                    
                    ye = e.Y;
                    rectangle = true;
                                      
                    PBimg.Invalidate();
                }
            }
        }

        private void PBimg_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;

            if (action)
            {
                xe = e.X;
                ye = e.Y;

                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;

               isMousePressed = false;
                Refresh();
                this.Invalidate();
            }
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = "";
            sfd.Filter = "PNG(*.png)|*.png;|Bmp(*.bmp)|bmp;|Jpeg(*.jpg)|*.jpg";
            ImageFormat format = ImageFormat.Png;
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string ext = System.IO.Path.GetExtension(sfd.FileName);
                switch (ext)
                {
                    case ".jpg":
                        format = ImageFormat.Jpeg;
                        break;
                    case ".bmp":
                        format = ImageFormat.Bmp;
                        break;
                }
                img.Save(sfd.FileName, format);
                MessageBox.Show("Picture has been saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else {
                MessageBox.Show("Picture was not saved", "Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PBimg_Click(object sender, EventArgs e)
        {

        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String helpmsg = "You can zoom in and zoom out the image as you like."
                + "\n" 
                + "You can also export image as a PNG or JPG"
                 + "\n"
                + "You can also save and open the state of your image"
                + "\n"
                +"Developer: Aashutosh Shrestha";
            MessageBox.Show(helpmsg,"Help",MessageBoxButtons.OK,MessageBoxIcon.None);
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PBimg.Refresh();
            PBimg.Update();


            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            j = 0;
            mandelbrot();
            rectangle = false;

            isMousePressed = false;
            Refresh();
            this.Invalidate();
            MessageBox.Show("Resetted Succesfully", "Resetted", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "XML files (*.xml)|*.xml", // only allow xml files to be saved
                CreatePrompt = true, // make user confirm they want to save file
                FileName = "fractal-state" // default name of xml file to be saved
            };

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var converter = TypeDescriptor.GetConverter(typeof(Bitmap));
                   
                    var document = new XDocument( // define xml tree
                        new XElement("state", // parent node - the identifier 
                        new XElement("xstart", xstart),
                        new XElement("ystart", ystart),
                        new XElement("xzoom", xzoom),
                        new XElement("yzoom", yzoom),
                        new XElement("j", j)
                    ));

                    document.Save(saveFileDialog.FileName); // save document to the selected path
                    string selectedFileExtension = Path.GetExtension(saveFileDialog.FileName); // get file extension (in this case xml)
                    message.Text = String.Format("Successfully saved fractal state at {0} in {01} format.", saveFileDialog.FileName, selectedFileExtension);
                }
                catch (Exception ex)
                {
                   message.Text = String.Format("Failed to save fractal state. {0}", ex.Message); // failed to convert to xml and save, display error message
                }
            }
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            j = 0;
            
            message.Text = "Selected Palette: Red";
            mandelbrot();
            Refresh(); // redraw picture and child components
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            j = 60;
            
            message.Text = "Selected Palette: Green";
            mandelbrot();
            Refresh();
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            j = 150;
            
            message.Text = "Selected Palette: Blue";
            mandelbrot();
            Refresh();
        }

        private void yellowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            j = 30;
           
            message.Text = "Selected Palette: Yellow";
            mandelbrot();
            Refresh();
        }

        private void orangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            j = 10;
           
            message.Text = "Selected Palette: Orange";
            mandelbrot();
            Refresh();
        }

        private void turquoiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            j = 120;
           
            message.Text = "Selected Palette: Turquoise";
            mandelbrot();
            Refresh();
        }

        private void purpleToolStripMenuItem_Click(object sender, EventArgs e)
        {

            j = 190;
           
            message.Text = "Selected Palette: Purple";
            mandelbrot();
            Refresh();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "XML files (*.xml)|*.xml" // only display xml files in directory
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Added as a precaution as the Filter will only display xml files anyway
                if (!String.Equals(Path.GetExtension(openFileDialog.FileName), ".xml", StringComparison.OrdinalIgnoreCase))
                {
                    MessageBox.Show("You must select an XML file.", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                    return;
                }

                try
                {
                    var streamReader = new StreamReader(openFileDialog.FileName); // initialise stream reader to read selected xml file
                    using (streamReader) // using statement disposes of system resources automatically
                    {
                        var document = new XmlDocument();
                        document.Load(openFileDialog.OpenFile());
                        var xnList = document.SelectNodes("/state"); // select parent node

                        foreach (XmlNode xmlNode in xnList) // loop through child nodes to access stored bitmap attributes
                        {
                            xstart = Convert.ToDouble(xmlNode["xstart"].InnerText);
                            ystart = Convert.ToDouble(xmlNode["ystart"].InnerText);
                            xzoom = Convert.ToDouble(xmlNode["xzoom"].InnerText);
                            yzoom = Convert.ToDouble(xmlNode["yzoom"].InnerText);
                            j = Convert.ToInt32(xmlNode["j"].InnerText);
                        }
                        mandelbrot();
                        Refresh();  // redraw picture and child components // repaint(); // djm original Java

                        string selectedFileExtension = Path.GetExtension(openFileDialog.FileName); // get file extension (in this case xml)
                        message.Text = String.Format("Successfully loaded fractal state from {0} at {1}.", selectedFileExtension, openFileDialog.FileName);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to Load Fractal State:"+"\n"+ex.Message, "Load Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                     // failed to load, display error message
                }

            }
        }

        public struct HSBColor

        {

            float h;
            float s;
            float b;
            int a;

            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }


            public static Color FromHSB(HSBColor hsbcolor)
            {
                float r = hsbcolor.b;
                float g = hsbcolor.b;
                float b = hsbcolor.b;
                if (hsbcolor.s != 0)
                {
                    float max = hsbcolor.b;
                    float dif = hsbcolor.b * hsbcolor.s / 255f;
                    float min = hsbcolor.b - dif;

                    float h = hsbcolor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }

                if (finished == true)
                {
                    return Color.FromArgb
                    (
                        hsbcolor.a,
                        (int)30,
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))


                        );
                }

                return Color.FromArgb
                    (
                        hsbcolor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            

            //PanelToDisplayImage.Controls.
        }
        private void startToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            timer1.Start();
            timer1.Interval = 600;
            message.Text = "Color Cycling was started in Normal Speed";
        }

        private void x2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Interval = 600;
            message.Text = "Color Cycling Speed: Normal";
        }

        private void x3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Interval = 300;
            message.Text = "Color Cycling Speed: x2";

        }

        private void x4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Interval = 100;
            message.Text = "Color Cycling Speed: x3";

        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            timer1.Dispose();
            message.Text = "Color Cycling Stopped";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            mandelbrot();
            Refresh(); // redraw picture and child components

            if (j == 240) // if j reaches 240 (black) then set variable to cycle backwards 
            {
                cycleForwards = false;
            }

            if (j == 0) // if j reaches 0 (red) then set variable to cycle forwards 
            {
                cycleForwards = true;
            }

            if (cycleForwards)
            {
                j++; // cycle colours from light to dark
            }
            else
            {
                j--; // cycle colours from dark to light
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
           
        }
        public void init() // all instances will be prepared
        {
            
            x1 = PBimg.Width;
            y1 = PBimg.Height;
            xy = (float)x1 / (float)y1;
            img = new Bitmap(x1, y1);
            g1 = Graphics.FromImage(img);
        }
        public void destroy() // delete all instances 
        {
            if (finished)
            {
                img = null;
                g1 = null;
                GC.Collect(); 
            }
        }
        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            pen = new Pen(Color.Red, 2);
            mandelbrot();
        }
        public void stop()
        {
        }
        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;
            pen = new Pen(Color.Red, 2);
            action = false;
            
            c1 = Cursors.WaitCursor;


           
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes
                        HSBCol = new HSBColor(h * 255, 0.8f * 255, b * 255);
                        Color color = HSBColor.FromHSB(HSBCol);
                        pen = new Pen(color);
                        alt = h;
                    }
                    g1.DrawLine(pen, x, y, x + 1, y);
                }
           
            c2 = Cursors.Cross;
            action = true;
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            var jReference = j;
            while ((jReference < MAX) && (m < 4.0))
            {
                jReference++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)jReference / (float)MAX;
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
            message.Font = new Font("Arial", 8, FontStyle.Bold);
            message.ForeColor = System.Drawing.Color.Green;
        }
        private void MouseDown(object sender, MouseEventArgs e)
        {
            
            if (action)
            {
                xs = e.X;
                ys = e.Y;
            }
        }
        private void MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;
            
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                Refresh();
            }
        }
        private void MouseMove(object sender, MouseEventArgs e)
        {
            
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                Refresh();
            }
        }
       
        


    }

}
